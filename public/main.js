var quoteArray = ["A forward-thinking engineer with a dual degree in Data Sciences and Project Management from CentraleSupélec and IAE Metz, complemented by a Bachelor's degree from the University of Lille. With diverse project experience ranging from robotics for minimally invasive surgery to data-driven solutions, I bring a versatile skill set that is well-equipped to drive project success. My strengths lie in project management, teamwork, and problem-solving, skills I'm looking forward to sharpening further during my upcoming consulting internship. My objective is to leverage my abilities to optimise business strategies and deliver impactful solutions."];
var textPosition = 0;
var speed = 40;

typewriter = () => {
  var textElement = document.querySelector("#profil_text");
  textElement.innerHTML = quoteArray[0].substring(0, textPosition) + '<span>\u25AE</span>';
  textElement.classList.add("terminal"); // Add the 'terminal' class for the background box

  if (textPosition++ != quoteArray[0].length)
    setTimeout(typewriter, speed);
}

window.addEventListener("load", typewriter);



// Get all the timeline containers
var timelineContainers = document.getElementsByClassName('container_timeline');

// Loop through each container and add hover listener
for (var i = 0; i < timelineContainers.length; i++) {
  timelineContainers[i].addEventListener('mouseover', function() {
    this.getElementsByClassName('content')[0].style.boxShadow = "0 4px 8px 0 rgba(0,0,0,0.2)";
  });
  timelineContainers[i].addEventListener('mouseout', function() {
    this.getElementsByClassName('content')[0].style.boxShadow = "none";
  });
}


// When the user scrolls the page, execute myFunction
window.onscroll = function() {myFunction()};

// Get the navbar
var navbar = document.getElementById("navbar");

// Get the offset position of the navbar
var sticky = navbar.offsetTop;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}


const sections = document.querySelectorAll('.scroll-animation');

const options = {
  threshold: 0.1, // adjust this value to when you want the animation to start (1.0 means the entire section is in the viewport, 0.5 means half of it, etc.)
};

const observer = new IntersectionObserver(function(entries, observer) {
  entries.forEach(entry => {
    if(entry.isIntersecting) {
      entry.target.classList.add('appear');
    } else {
      entry.target.classList.remove('appear');
    }
  });
}, options);

sections.forEach(section => {
  observer.observe(section);
});

