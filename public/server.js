require('dotenv').config();
const express = require('express');
const sqlite3 = require('sqlite3').verbose();
const crypto = require('crypto');
const path = require('path');

const app = express();
const port = 3000;

// Middleware to parse the request body
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Serve static files (HTML, CSS, etc.)
app.use(express.static(path.join(__dirname, '/')));

// Serve CV files
app.use('/CV', express.static(path.join(__dirname, '/CV')));

// Serve image files
app.use('/image', express.static(path.join(__dirname, '/image')));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

// Encrypt the email address
const encryptEmail = (email) => {
  const cipher = crypto.createCipher('aes-256-cbc', process.env.ENCRYPTION_KEY);
  let encrypted = cipher.update(email, 'utf8', 'hex');
  encrypted += cipher.final('hex');
  return encrypted;
};

// Store the form data in the database
app.post('/submit-form', (req, res) => {
  const { name, email, message } = req.body;

  // Encrypt the email address
  const encryptedEmail = encryptEmail(email);

  // Store the form data in the database
  const dbPath = path.join(__dirname, 'formdata.db');
  const db = new sqlite3.Database(dbPath);

  const insertQuery = `INSERT INTO formdata (name, email, message) VALUES (?, ?, ?)`;
  const values = [name, encryptedEmail, message];

  db.run(insertQuery, values, function (err) {
    if (err) {
      console.error(err);
      res.status(500).send('Error storing form data');
    } else {
      console.log('Form data stored successfully');
      res.status(200).send('Form submitted successfully');
    }
  });
  // Retrieve and log the stored form data
  const selectQuery = `SELECT * FROM formdata`;
  db.all(selectQuery, [], (err, rows) => {
    if (err) {
      console.error(err);
    } else {
      console.log('Stored form data:');
      console.log(rows);
    }
  });

  

  db.close();
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
